from typing import Any
from django.db import models
from django.contrib.auth.models import AbstractUser


class Mutant(AbstractUser):
    nickname = models.CharField(blank=True, max_length=64)
    avatars = models.ImageField(
        'Картинка солнышка',
        upload_to='mutants/',
        blank=True,
    )

    STATUS_CHOICES = [
        ("activ", "Актив"),
        ("old", "Мутоолдовец"),
        ("intern", "Стажер"),
        ("friend", "Друг команды"),
    ]

    status = models.CharField(choices=STATUS_CHOICES, default="activ", max_length=16)

    def __str__(self):
        if self.nickname:
            return self.nickname
        return self.first_name


class Sun(models.Model):
    name = models.CharField(max_length=32,
                            unique=True,
                            verbose_name="Название солнышка")
    image = models.ImageField(
        'Картинка солнышка',
        upload_to='suns/image',
        blank=True,
        null=True
    )
    description = models.TextField(blank=True, null=True)
    mutant = models.ManyToManyField(Mutant, verbose_name='мутант', blank=True, related_name='suns')
