from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Mutant, Sun
from .forms import MutantCreationForm, MutantUserChangeForm

@admin.register(Mutant)
class MutantAdmin(UserAdmin):
    model = Mutant
    list_display = ('username', 'nickname')
    list_display_links = ('username', 'nickname')
    fields = (
        'username', 'first_name', 'last_name', 'email', 'nickname', 'avatars', 'status'
    )
    fieldsets = None

@admin.register(Sun)
class SunAdmin(admin.ModelAdmin):
    model = Sun
    list_display = ('name',)
