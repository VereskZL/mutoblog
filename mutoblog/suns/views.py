from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView

from .models import Mutant, Sun

from .forms import MutantCreationForm

# Create your views here.
def index(request):
    context = {
        'activ': status_mutant('activ'),
        'old': status_mutant('old'),
        'intern': status_mutant('intern'),
        'friend': status_mutant('friend'),
    }
    template = 'index.html'
    return render(request, template, context)

def status_mutant(status):
    mutants = Mutant.objects.filter(status=status)
    return mutants

class SignUpView(CreateView):
    form_class = MutantCreationForm
    success_url = reverse_lazy("login")
    template_name = "signup.html"

def profile(request, username):
    user = get_object_or_404(Mutant, username=username)
    suns = Sun.objects.filter(mutant=user)

    context = {
        'mutant': user,
        'suns': suns,
    }
    return render(request, 'profile.html', context)

# TODO настроить админку
