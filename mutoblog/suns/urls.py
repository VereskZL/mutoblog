from django.contrib.auth.views import (LogoutView, LoginView,
                                       PasswordChangeView,
                                       PasswordChangeDoneView,)
from django.urls import path
from . import views

app_name = 'suns'
urlpatterns = [
    path("signup/", views.SignUpView.as_view(), name="signup"),
    path(
        'logout/',
        LogoutView.as_view(template_name='users/logged_out.html'),
        name='logout'),
    path(
        'login/',
        LoginView.as_view(template_name='login.html'),
        name='login'),
    path('profile/<str:username>/', views.profile, name='profile'),
]
