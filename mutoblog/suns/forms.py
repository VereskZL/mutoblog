from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import Mutant


class MutantCreationForm(UserCreationForm):

    class Meta:
        model = Mutant
        fields = ('email', 'nickname', 'username')


class MutantUserChangeForm(UserChangeForm):

    class Meta:
        model = Mutant
        fields = ("email",)
